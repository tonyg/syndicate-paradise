#!/bin/sh
SERVICEID=${1:-paradise}
SERVICEURL=${2:-wss://paradise.demo.leastfixedpoint.com/}
syndicate-macaroon noise --random --service $SERVICEID > config/server-key.pr
cat > web/client-key.js <<EOF
const routeToServer = \`
<route
  [<ws "$SERVICEURL">]
  <noise { key: $(preserves-tool --select '. key' < config/server-key.pr),
           service: $SERVICEID }>
>\`;
EOF
