async function createFreshRoot(resume, realm) {
  realm.roots.rootVessel = resume(await realm.createAndSetupVessel(resume, {
    name: `song of power for realm number ${Math.trunc(Math.random() * 100000000)}`,
    traits: ['root'],
  }));
  realm.roots.operating = realm.roots.rootVessel;
}

spawn named Symbol.for('operator') {
  currentSyndicateFacet.preventInertCheck(); // TODO: remove?

  const ui = create new Syndicate.Dataspace({ tracer: dataspaceTracer('UI') });

  suspend(async resume => {
    let realm = resume(await Realm.loadFromLocalStorage(resume, Spirit.animate));
    if (!realm) {
      realm = new Realm(Spirit.animate);
      resume(await createFreshRoot(resume, realm));
    }

    if (!realm.roots.operating) {
      realm.roots.operating = realm.roots.rootVessel;
    }

    // spawn named Symbol.for('uplink') {
    //   contactRemote(decodeStandardRoute(routeToServer.trim()), o => {
    //     console.log('contacted remote', o);
    //     sendCommand(realm.roots.rootVessel, realm.roots.rootVessel, ['goto', 'link', o]);
    //   });
    // }

    at ui {
      const operator = P.Operator({
        connected: activeAttenuation(ui, P.toConnected),
        avatars: activeAttenuation(ui, P.toAvatar),
        commandFormats: activeAttenuation(ui, P.toCommandFormat),
        events: activeAttenuation(ui, P.toEvent),
      });
      assert O.Operating(realm.roots.operating);
      during O.Operating($o) => at o { assert operator; }

      const enabledCommands = new CommandSet();
      during $f(P.CommandFormat(_)) => {
        const cf = enabledCommands.addCommand(f);
        const rendering = t => renderCommandFormatPattern(t, cf.commandFormat.pattern);
        new Widget(t => t`<li><span class="command-synopsis">${rendering(t)}</span></li>`)
          .setParent('#enabled');
        on stop enabledCommands.deleteCommand(f);
      }

      assert makeCommandFormat(['become'], '+operator become ?agent');
      during P.CommandInvocation({ id: $_id, body: ['become', _], arena: $arena }) => {
        at arena {
          once asserted P.Role({ role: 'agent', body: O.Operating($o) }) => {
            realm.roots.operating = o;
          }
        }
      }

      const environment = new CommandEnvironment();
      environment.ambients.operator = activeAttenuation(ui, P.toCommandInvocation);

      function trackAvatars(category, label, container) {
        during P.Avatar({ category: category, name: _ /* TODO: make it so this wildcard doesn't have to exist */ }) => {
          new Widget(t => t`<span>${label}</span>`).setParent(`${container} > h3`);
          during P.Avatar({ category: category, name: $name, handler: $handler }) => {
            const uniqueName = recordUniqueName(environment.scope, name, handler);
            new Widget(t => t`<li>${anArticle(uniqueName)}</li>`).setParent(`${container} > ul`);
            on stop delete environment.scope[uniqueName];
          }
        }
      }

      trackAvatars(P.Category.visible(), 'you can see:', '#sight');
      trackAvatars(P.Category.held(), 'you are holding:', '#inventory');

      during P.Avatar({ category: P.Category.agent(), name: $name, handler: $handler }) => {
        environment.ambients.agent = handler;
        on stop if (environment.ambients.agent === handler) delete environment.ambients.agent;
        new Widget(t => t`<span>you are ${anArticle(name)}</span>`).setParent('#you-are .agent');
      }

      during P.Avatar({ category: P.Category.location(), name: $name, handler: $handler }) => {
        environment.ambients.location = handler;
        on stop if (environment.ambients.location === handler) delete environment.ambients.location;
        new Widget(t => t`<span> in ${anArticle(name)}</span>`).setParent('#you-are .location');
      }

      on retracted P.Connected() => {
        // Vessel destroyed while we were inhabiting it! Switch to the rootVessel, unless that
        // was the one we were operating, in which case create and switch to a fresh root.

        if (realm.roots.operating !== realm.roots.rootVessel) {
          realm.roots.operating = realm.roots.rootVessel;
        } else {
          suspend(async resume => resume(await createFreshRoot(resume, realm)));
        }
      }

      receiveAndDisplayEvents(ui);

      const input = document.getElementById('command');
      input.focus();
      const submitHandler = submitEvent => {
        submitEvent.preventDefault();

        const cmd = input.value;
        input.value = '';
        document.getElementById('main-section')?.scrollIntoView(false);
        input.focus();
        if (!cmd) return;

        let b = enabledCommands.match(cmd[0] === ' ' ? `say${cmd}` : cmd, environment);
        // TODO: do something sensible with unmatched input

        if (b) {
          console.log(b.roles , '<--', b.body);
          currentSyndicateFacet.turn(() => sendCommand(b.roles, b.body));
        }
      };
      document.getElementById('input').addEventListener('submit', submitHandler);
      on stop document.getElementById('input').removeEventListener('submit', submitHandler);
    }
  });
}

function renderCommandFormatPattern(t, pattern) {
  return pattern.flatMap(pred => {
    switch (pred._variant) {
      case 'fixed':
        return [pred.options.length > 1 ? '(' + pred.options.join(' | ') + ')' : pred.options[0]];
      case 'role':
        return [t`<i>${pred.value}</i>`];
      case 'free':
        return ['—'];
      case 'ambient':
      default:
        return [];
    }
  }).flatMap((x, i) => i === 0 ? [x] : [' ', x]);
}
