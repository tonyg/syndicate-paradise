# Operations and their protocols

## Speaking, emoting

Scope: the location holding the agent.

Permission: any agent, while present in the location.

Data: entirely first-order.

## Destroy

Scope: a single object. Reflective operation.

Permission: holder of the mirror for the object. Possibly have to be able to see/touch the
object? Depends how "in-game" the operation should be. Probably want two operations, one for
in-game and one primitive.

Data: none.

## Transform

Action: change an object's name.

Permission: holder of the mirror.

Data: first-order.

## Goto

Multiple variations:
 - graft a location into a remote realm on initial connection
 - give something to another vessel
 - put something down in your current location
 - pick something up from your current location
 - leave a location for its surrounding location
 - enter some local object in your current location
 - enter anything you can name, subject to containment loops, though these might not actually be a problem
 - take a door/exit from the current location to somewhere else
 - move a thing from where it is to inside a new thing
 - move a thing from inside something to some other place
 - go home

## Set home

Change an object's home location.

Permission: holder of the mirror for the object to be changed; new location must be willing to
accept object generally. Or, perhaps the willingness-to-accept test is just done at the time of
going home (since we probably have to handle the failure case anyway).

Data: higher-order (a vessel reference).

## Set property

Reflective. Mirror-holder.

## Create vessel

Need to hold a creation object. It has to have enough "charge" left. Need to have permission to
put the new thing somewhere.
