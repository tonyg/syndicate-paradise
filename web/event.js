const EVENT_SIGNAL_TIMEOUT = 1000;

function sendEvent(id0, vessel, agent, ... pieces) {
  const id = id0 ?? Syndicate.genUuid();
  spawn {
    at (vessel || void 0) {
      assert P.Event({
        id: id,
        unixTimeMs: Math.trunc(+new Date()),
        description: [
          _the(agent, true),
          ... pieces.map(d => typeof d === 'string' ? P.EventItem.string(d) : d)
        ],
      });
    }
    Syndicate.Turn.active.after(EVENT_SIGNAL_TIMEOUT, () => { stop {} });
  }
  return id;
}

function _a(vessel, capitalized = false) {
  return P.EventItem.name({ article: P.Article.a(), capitalized, vessel });
}
function _the(vessel, capitalized = false) {
  return P.EventItem.name({ article: P.Article.the(), capitalized, vessel });
}
function _name(vessel, capitalized = false) {
  return P.EventItem.name({ article: P.Article.none(), capitalized, vessel });
}

const _thePlace = (x, capitalized = false) => x ? _the(x, capitalized) : 'nowhere';
const _theThing = (x, capitalized = false) => x ? _the(x, capitalized) : 'nothing';

const seenLogItems = new Preserves.Set();

function receiveAndDisplayEvents(scope) {
  at scope {
    on asserted P.Event({ id: $id, unixTimeMs: $postTime, description: $description }) => {
      if (seenLogItems.has(id)) return;
      seenLogItems.add(id);

      spawn named Syndicate.Record(Symbol.for('log'), [id]) {
        on stop seenLogItems.delete(id);

        const pieces = description.map(d0 => {
          const d = P.toEventItem(d0);
          if (d === void 0) return () => '';
          switch (d._variant) {
            case 'string':
              return () => d.value;
            case 'name':
              if (d.vessel === /* TODO figure this out */ '?!?!?!?!?!?!') {
                return d.capitalized ? () => 'You' : () => 'you';
              } else {
                field name = '' + d.vessel;
                at d.vessel { on asserted P.Name($n) => name.value = n; }
                return () => {
                  switch (d.article._variant) {
                    case 'none': return name.value;
                    case 'a': return d.capitalized ? AnArticle(name.value) : anArticle(name.value);
                    case 'the': return d.capitalized ? TheArticle(name.value) : theArticle(name.value);
                    default: return '';
                  }
                };
              }
            default:
              return () => '';
          }
        });

        const ts = new Date(postTime);
        const widget = new Widget(t => t`<p class="event">
            <span class="timestamp" title="${ts.toLocaleDateString()}">${ts.toLocaleTimeString()}</span>
            ${pieces.map(p => p())}
          </p>`).setParent('#log');
      }
    }
  }
}
