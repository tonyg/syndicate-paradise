const PARTICLES = 'a the an at in into to by as'.split(' ');
function removeParticles(words) {
  let i = 0;
  while (i < words.length && PARTICLES.indexOf(words[i]) !== -1) i++;
  return words.slice(i);
}

function makeCommandFormat(prefix, pattern) {
  const preds = [];
  const patternWords = pattern.split(/ +/);
  for (const w of patternWords) {
    switch (w[0]) {
      case '?': preds.push(P.MatchPredicate.role(w.slice(1))); break;
      case '-': preds.push(P.MatchPredicate.free()); break;
      case '+': preds.push(P.MatchPredicate.ambient(P.asScope(Symbol.for(w.slice(1))))); break;
      default: preds.push(P.MatchPredicate.fixed(w.split('|'))); break;
    }
  }
  return P.CommandFormat({ prefix, pattern: preds });
}

function nameFromString(str) {
  return nameFromWords(str.split(/ +/));
}

function nameFromWords(words) {
  return removeParticles(words).join(' ');
}

function searchForItem(items, name, ks, kf) {
  const lcName = name.toLowerCase().trim();
  if (!lcName) return kf();
  const matches = Object.keys(items).filter(i => i.toLowerCase().indexOf(lcName) !== -1);
  if (matches.length === 0) return kf();
  matches.sort((a, b) => a.length > b.length);
  return ks(... matches.map(n => items[n]));
}

function recordUniqueName(index, name, handler) {
  if (name in index && index[name] !== handler) {
    let i = 2;
    while (true) {
      const candidate = `${name} #${i}`;
      if (!(candidate in index && index[candidate] !== handler)) {
        name = candidate;
        break;
      }
      i++;
    }
  }
  index[name] = handler;
  return name;
}

function anArticle(x) {
  if (x.slice(0, 1).toUpperCase() === x.slice(0, 1)) {
    return x;
  }
  if (/#\d+$/.exec(x)) {
    // a "uniquely named" variation; see recordUniqueName
    return x;
  }
  if (x.startsWith('some ')) {
    return x;
  }
  if ('aeiou'.indexOf(x.slice(0, 1)) === -1) {
    return 'a ' + x;
  } else {
    return 'an ' + x;
  }
}

function AnArticle(x) {
  const y = anArticle(x);
  return y.slice(0, 1).toUpperCase() + y.slice(1);
}

function theArticle(x) {
  if (x.slice(0, 1).toUpperCase() === x.slice(0, 1)) {
    return x;
  }
  if (/#\d+$/.exec(x)) {
    // a "uniquely named" variation; see recordUniqueName
    return x;
  }
  if (x.startsWith('some ')) {
    return 'the ' + x.slice(5);
  }
  return 'the ' + x;
}

function TheArticle(x) {
  const y = theArticle(x);
  return y.slice(0, 1).toUpperCase() + y.slice(1);
}

class CommandSet {
  constructor() {
    this.commands = new Preserves.KeyedDictionary();
  }

  addCommand(c0) {
    const c = P.toCommandFormat(c0);
    if (c === void 0) return;
    const cf = new CompiledCommandFormat(c);
    this.commands.set(c0, cf);
    return cf;
  }

  deleteCommand(c) {
    this.commands.delete(c);
  }

  match(commandString, env) {
    for (const cf of this.commands.values()) {
      const b = cf.match(commandString, env);
      if (b) return b;
    }
    return null;
  }
}

class CommandEnvironment {
  constructor(scope = {}, ambients = {}) {
    this.scope = scope;
    this.ambients = ambients;
  }
}

class CommandBuilder {
  constructor(format, matches, env) {
    this.format = format;
    this.matches = matches;
    this.capture = 1;
    this.body = this.format.commandFormat.prefix.slice();
    this.roles = {};
    this.env = env;
  }

  nextCapture() {
    return this.matches[this.capture++].trim();
  }

  pushValue(v) {
    this.body.push(v);
    return true;
  }

  pushRole(roleName) {
    const capture = this.nextCapture();
    const name = nameFromString(capture);
    const handler = searchForItem(this.env.scope, name, e => e, () => false);
    if (!handler) return false;
    this.roles[roleName] = handler;
    return this.pushValue(capture);
  }

  pushFree() {
    return this.pushValue(this.nextCapture());
  }

  pushAmbient(whichAmbient) {
    const handler = this.env.ambients[whichAmbient];
    if (handler === void 0) return false;
    this.roles[whichAmbient] = handler;
    return true;
  }
}

class CompiledCommandFormat {
  constructor(commandFormat /* parsed */) {
    this.commandFormat = commandFormat;
    this.matchActions = [];

    const re_pieces = [];
    for (const pred of this.commandFormat.pattern) {
      switch (pred._variant) {
        case 'fixed': // TODO sanitize the pieces by removing metacharacters etc
          re_pieces.push(' +(?:' + pred.options.join('|') + ')');
          break;
        case 'role': {
          re_pieces.push(' +(.+)');
          this.matchActions.push(b => b.pushRole(pred.value));
          break;
        }
        case 'free':
          re_pieces.push('(.*)');
          this.matchActions.push(b => b.pushFree());
          break;
        case 'ambient': {
          this.matchActions.push(b => b.pushAmbient(pred.value._variant));
          break;
        }
      }
    }
    const re_str0 = re_pieces.join('').trim();
    const re_str = re_str0[0] === '+' ? re_str0.slice(1) : re_str0;
    this.re = new RegExp('^' + re_str + '$', 'i');
  }

  match(commandString, env) {
    const m = this.re.exec(commandString);
    if (!m) return null;

    const b = new CommandBuilder(this, m, env);
    for (const ac of this.matchActions) {
      if (!ac(b)) return null;
    }

    return b;
  }
}
