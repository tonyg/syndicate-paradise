function Scope(onNewField = void 0, onDeleteField = void 0) {
  const facet = Syndicate.Turn.activeFacet;
  const g = facet.actor.dataflowGraph;
  const inFacet = a => {
    const active = Syndicate.Turn.activeFacet;
    if (active === facet || active === facet.parent) {
      a();
    } else {
      facet.turn(a);
    }
  };
  const p = (storage, prop, initialValue = void 0) => {
    return storage[prop] ??= (() => {
      const f = new Syndicate.Dataflow.Field(g, initialValue, prop);
      inFacet(() => onNewField?.(f));
      return f;
    })();
  };
  return new Proxy({}, {
    get: (storage, prop) => {
      return p(storage, prop).value;
    },
    set: (storage, prop, value) => {
      p(storage, prop, value).value = value;
      return true;
    },
    deleteProperty: (storage, prop) => {
      const f = storage[prop];
      if (f === void 0) return;
      delete storage[prop];
      inFacet(() => onDeleteField?.(f));
      return true;
    },
    ownKeys: (storage) => {
      return Object.keys(storage);
    }
  });
}

class Realm {
  constructor(bootVessel, realmId = Realm.freshRealmId()) {
    this.realmId = realmId;
    localStorage.setItem('r', realmId);
    this.vesselIds = new WeakMap();
    this.roots = new Scope(f => {
      dataflow {
        localStorage.setItem(`g ${f.name}`, this.stringify(f.value));
      }
    }, f => localStorage.removeItem(`g ${f.name}`));
    this.bootVessel = bootVessel;
    this.maxVesselId = -1;
  }

  stringify(value) {
    return value === void 0 ? '' : Preserves.stringify(value, { embeddedWrite: this });
  }

  encode(s, v) {
    new Preserves.Encoder(s).push(this.toValue(v));
  }

  toValue(v) {
    const id = this.vesselIds.get(v.target);
    if (id === void 0) return false;
    return [id, ... fromJS(v.attenuation ?? [])];
  }

  createVessel(vesselId = this.maxVesselId + 1) {
    this.maxVesselId = Math.max(this.maxVesselId, vesselId);
    return new Promise(k => {
      spawn named `v-${vesselId}` {
        const vessel = create new Syndicate.Dataspace({ tracer: dataspaceTracer(`v-${vesselId}`) });
        this.vesselIds.set(vessel.target, vesselId);
        k(vessel);
        localStorage.setItem(`f ${vesselId}`, true);
        on stop localStorage.removeItem(`f ${vesselId}`);
        at vessel {
          once message [$persistentFields, $context] => {
            const scope = new Scope(f => {
              dataflow {
                localStorage.setItem(`f ${vesselId} ${f.name}`, this.stringify(f.value));
              }
            }, f => localStorage.removeItem(`f ${vesselId} ${f.name}`));
            for (const [k, v] of Object.entries(persistentFields)) {
              scope[k] = v;
            }
            on stop {
              for (const k of Object.keys(scope)) {
                delete scope[k];
              }
            }
            this.bootVessel(this, vesselId, vessel, scope, context);
          }
        }
      }
    });
  }

  async createAndSetupVessel(resume, persistentFields, context = {}) {
    const v = resume(await this.createVessel());
    at v {
      send message [persistentFields, context];
    }
    return v;
  }

  static freshRealmId() {
    return Syndicate.randomId(32);
  }

  static async loadFromLocalStorage(resume, bootVessel, context = {}) {
    const savedRealmId = localStorage.getItem('r');
    if (!savedRealmId) {
      return null;
    }

    const realm = new Realm(bootVessel, savedRealmId);

    const vesselPromises = [];
    const vesselFields = {};
    const rootStrings = {};
    for (const key of Object.keys(localStorage)) {
      if (key.startsWith('f ')) {
        const [vesselIdStr, fieldName] = key.slice(2).split(' ');
        const vesselId = parseInt(vesselIdStr, 10);
        if (fieldName === void 0) {
          vesselPromises.push(realm.createVessel(vesselId));
        } else {
          (vesselFields[vesselId] ??= []).push([fieldName, localStorage.getItem(key)]);
        }
      } else if (key.startsWith('g ')) {
        rootStrings[key.slice(2)] = localStorage.getItem(key);
      } else {
        // skip this key
      }
    }

    const loaded = new Map(); // vessel ID --> vessel ref

    const allVessels = resume(await Promise.all(vesselPromises));

    for (const vessel of allVessels) {
      const vesselId = realm.vesselIds.get(vessel.target);
      loaded.set(vesselId, vessel);
    }

    const decodeEmbeddedVessel = {
      decode(s) {
        return this.fromValue(new Preserves.Decoder(s).next());
      },
      fromValue(v) {
        if (v === false) return false;
        const r = loaded.get(v[0]);
        if (!r) {
          console.warn('decodeEmbeddedVessel: no loaded vessel for id', v);
          return false;
        }
        const attenuation = v.slice(1).map(Syndicate.Schemas.sturdy.asCaveat);
        return R.attenuate(r, ... attenuation);
      },
    };

    const parse = str =>
          str === '' ? void 0 :
          Preserves.parse(str, { embeddedDecode: decodeEmbeddedVessel });

    for (const k of Object.keys(rootStrings)) {
      const v = parse(rootStrings[k]);
      realm.roots[k] = v;
    }

    for (const vessel of allVessels) {
      const vesselId = realm.vesselIds.get(vessel.target);
      const fields = {};
      for (const [fieldName, valueStr] of (vesselFields[vesselId] ?? [])) {
        fields[fieldName] = parse(valueStr);
      }
      at vessel {
        send message [fields, context];
      }
    }

    return realm;
  }

  isLocal(ref) {
    return this.vesselIds.has(ref.target);
  }
}
