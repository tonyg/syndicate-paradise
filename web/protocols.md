Being a thing-in-the-world
 - persistent

Being able to be put somewhere
 - "here, have a capability to a being-a-space"
 - X grants permission to put X somewhere to ...?
     - holders of permissions-to-put conspire to revoke previous being-a-space capabilities

Inhabiting a space
 - present an appearance (name/description)
 - offer affordances for interaction (new verbs, callable methods (a capability!))

Being a space
 - conspire with other spaces to maintain 0-or-1 location per object
 - what kind of space is it? A room? A person's inventory? A container object?

Moving from space to space
 - Doors, if active, can check that the thing to be moved is in the source location before
   bothering to check if the target location will accept the thing.
 - In fact they might have to do so if the required capabilities are only accessible that way
 - The net effect could be that, absent special powers, you have to already *be* in some place
   to *move* to some other place.

Able to be an avatar
 - Zero or more agents driving is the default
 - Some commands available only to agents; others to second parties

---

Location: exports to contained objects REVOCABLE authority to
 - observe name bindings for peers and for the location itself
 - bind their own name (and no others)
 - request delivery of command invocations mentioning them (and no others)
 - deliver commands

Objects: exports to operators REVOCABLE authority to
 - observe name bindings for contained items and for the object itself
 - deliver commands
 - everything from the location's delegation
