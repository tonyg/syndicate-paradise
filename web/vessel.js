function sendCommand(roles, body0) {
  spawn {
    const id = Syndicate.genUuid();
    console.log(id, '=', stringify(fromJS(body)));
    const unixTimeMs = Math.trunc(+new Date());
    const body = fromJS(body0);
    const arena = create new Syndicate.Dataspace({ tracer: dataspaceTracer(id) });
    for (const [role, handler] of Object.entries(roles)) {
      const roleArena = create Syndicate.assertionObserver(a => {
        const o = Syndicate.toObserve(a);
        if (o) {
          if (o.pattern._variant === 'group'
              && o.pattern.type._variant === 'rec'
              && o.pattern.type.label === Symbol.for('role'))
          {
            return assertionRetractor(arena, a);
          }
        } else {
          return assertionRetractor(arena, P.Role({ role, body: a }));
        }
      });
      at handler {
        assert P.CommandInvocation({ id, unixTimeMs, role, body, arena: roleArena });
      }
    }
    Syndicate.Turn.active.after(EVENT_SIGNAL_TIMEOUT, () => { stop {} });
  }
}

function makeCommand(prefix, pattern, parser, handler) {
  return [makeCommandFormat(prefix, pattern), parser, handler];
}

const allTraits = {};

allTraits.root = {
  make: (spirit) => {
    // at (spirit.vessel) {
    //   assert makeCommandFormat(['endow'], '+agent:target endow ?* with -');
    //   assert makeCommandFormat(['strip'], '+agent:target strip - from ?*');
    // }
    return {
      commands: [
        makeCommand(['create'], '+agent create -', P.toCmdCreate, (role, cmd, arena) => {
          suspend(async resume => {
            const name = nameFromString(cmd.name);
            resume(await spirit.realm.createAndSetupVessel(resume, {
              name,
              traits: [],
            }, spirit.context));
            // HERE: location should be spirit.vessel
            // also: privileged channel for creator
            sendEvent(null, spirit.vessel, agent, ` created ${anArticle(name)}.`);
          });
          return true;
        }),
      ],
    };
  },
};

class Spirit {
  static animate(... args) {
    return new Spirit(... args);
  }

  constructor(realm, vesselId, vessel, state, context) {
    state.name ??= Syndicate.genUuid();
    state.location ??= false;
    state.traits ??= [];

    this.realm = realm;
    this.vesselId = vesselId;
    this.vessel = vessel;
    this.state = state;
    this.context = context;

    this.traits = state.traits.flatMap(b => allTraits[b]?.make?.(this) ?? []);
    at vessel {
      this.traits.forEach(t => t.commands.forEach(c => { assert c[0]; }));
    }

    this._trackLocation();
    this._presentAppearance();
    this._manageLocalNames();
    this._acceptCommands();
  }

  get location() {
    return this.state.location || void 0;
  }

  broadcast(id0, agent, ... description) {
    const id = sendEvent(id0, this.vessel, agent, ... description);
    return sendEvent(id, this.state.location, agent, ... description);
  }

  _trackLocation() {
    at (this.location) {
      assert P.Link(this.vessel);

      during P.Name(_) => {
        const locationPresent = this.state.location;
        on stop if (locationPresent === this.state.location) {
          this.state.location = this.realm.roots.rootVessel;
          sendEvent(null, this.state.location, this.vessel, ` came home.`);
        }
      }
    }
  }

  _presentAppearance() {
    at this.vessel {
      assert P.Name(this.state.name);
      assert P.Parent(this.state.location) when (this.state.location);
    }
  }

  _manageLocalNames() {
    at this.vessel {
      const localNames = {};
      during P.Link($other) => {
        at other {
          during P.Name($name) => {
            const entry = recordUniqueName(localNames, name, other);
            on stop delete localNames[entry.name];
            at this.vessel { assert entry; }
          }
        }
      }
    }
  }

  _acceptCommands() {
    at this.vessel {
      on asserted P.CommandInvocation({ id: $_id, role: $role, body: $body, arena: $arena }) => {
        for (const t of this.traits) {
          for (const [_commandFormat, parser, handler] of t.commands ?? []) {
            const cmd = parser(body);
            if (!cmd) continue;
            if (handler(role, cmd, arena)) return;
          }
        }
        sendEvent(null, arena, this.vessel, ` didn't understand how to do that.`);
      }
    }
  }
}
